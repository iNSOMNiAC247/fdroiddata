Categories:Internet
License:MPL-2.0
Author Name: Mozilla
Author Email:android-marketplace-notices@mozilla.com
Web Site:https://www.mozilla.org
Source Code:https://github.com/mozilla-mobile/focus-android
Issue Tracker:https://github.com/mozilla-mobile/focus-android/issues


Repo Type:git
Repo:https://github.com/mozilla-mobile/focus-android

Build:1.0,5
    commit=v1.0
    subdir=app
    gradle=klar,webkit
    rm=app/libs/*
    prebuild=sed -i -e '/focusCompile/d' -e '/geckoCompile/d' -e "s/compile(name: 'telemetry-2b0baee', ext: 'aar')/compile 'org.mozilla.telemetry:telemetry:1.0.0'/" build.gradle

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:1.0
Current Version Code:5
